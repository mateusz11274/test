import urllib3
from credentials import *
import re
import requests
import json
import time
import sys

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


########################################################################################################################
# ******Please Update the IP address, username and password for the APIC you want to acces in "credentials.py" file*****
# **********************************************************************************************************************
# Contact: mateusz.janowski@bt.com for any issues.
# The following program contains the backbone functions to execute automation scripts.
# The functions described below form the NorthBound API to the APIC.
# List of functions with corresponding arguments:
#
#	getCookie(url,username,password)				fetches authentication into fabric
#	GetTennantList()								fetches list of all tenants
#	GetAPList(tenant_name)							fetches list of all APs for given tenant
#	GetBDList(tenant_name)							fetches list of all BDs for given tenant
#	GetvrfList(tenant_name)							fetches list of all VRFs for given tenant
#	GetEPGList(tenant_name, apProfile)				fetches list of all EPGs for given tenant and AP
#	GetEPGBDList(tenant_name, apProfile, BDname)	fetches list of all EPGs using a given BD
#	GetBD4EPG(tenant_name, apProfile, EPGname)		fetches the BD for given epg
#	GetStaticPortsonEPGs(tenant_name, apProfile,EPGname)	RETURNS A LIST -> [pod, leaf, port, vlan(encap)]
#	GetBD4VRF(tenant_name, vrf)						fetches the BD for given VF
#	GetPhy4EPG(tenant_name, apProfile, EPGname)		fetches Physical Domains for given EPGs
#	GetVMMDomainsinEPGs("tenant01","AP1","EPG1")	fetches VMM Domains for given EPGs
#	GetContractList4EPGs(tenant_name,apProfile,EPGname) returns two lists: Consumed Contracts and Provided Contracts
#	GetContractList(tenant_name)					Returns a list of contracts present under Tenant
#	AddSubnetToBD(tenant_name, BDname, subnet,scope)Adds a subnet under tenant BD. PLEASE GIVE SCOPE. Can be "shared" or "private". In the context of VRFs
#	AddSubnetToEPG(tenant_name, apProfile, EPGname, subnet, scope)
#	AddTennant(tenant_name)							Creates a tenant, arg req. name of tenant
#	AddVRF2Tenant(tenant_name, vrf)					Creates a vrf under a tenant, networking tab
#	AddBD(BDname, tenant_name) 						Creates a bridge domain
#	AssociateVRFToBD(tenant_name, BDname, vrf)		ASSOCIates a vrf to a bd
#	AddEPG(tenant_name, apProfile, BDname, EPGname) Creates epg, given req. arguments
#	AddAP(tenant_name, apProfile)					Creates an application profile, specify tenant and a name for AP in argument
#	createContractandAssociate(apicIP,tenant_name,apProfile,provEPG,conEPG,contract_name) Creates and associates contracts to EPGs [NOT L3Out]
#	AssociateEPGasProvider(tenant_name,apProfile,epg,contract_name) Associate a contract to provider epg
#	AssociateEPGasConsumer(tenant_name,apProfile,epg,contract_name)	Associate a contract to consumer epg
#	DeleteEPGasConsumer(tenant_name,apProfile,epg,contract_name)	DE-Associate a contract to provider epg
#	DeleteEPGasProvider(tenant_name,apProfile,epg,contract_name)	DE-Associate a contract to consumer epg
#	ConsumeExternalContract(tenant_name,apProfile,epg,contract_name)	Consume external contracts (inter-tenant)
#	AssociatePhyDom2EPG(tenant_name, apProfile, EPGname, PHY)
#	AssociateVMDom2EPG(tenant, AP, epg, VM)
#	AssociateStaticPort2EPG(tenant_name, apProfile, EPGname, Vlan, pod,leafpath,port)
#	AssociateVPCPort2EPG(tenant_name, apProfile, EPGname, Vlan, pod,leafpath,port)	Associate vpc port to epg
#	addVLP(VLP_name, rangeFrom, rangeTo, allocationMode) Creates a VLAN Pool that can be associated later
#	attachAEP(AEP_name, Phy_name)					Creates and attaches Attachable Entity Profile to specified physical domain
#	createPHY(Phy_name, VLP_name)					creates PhysicalDomain and associates with specified VLP name
#	DelEPG(tenant_name, apProfile, EPGname)			Deletes epg
########################################################################################################################

# check for unregistered nodes
# url: https://11.11.11.160/api/node/class/fabricRsDecommissionNode.json
# response: {"totalCount":"0","subscriptionId":"72059140245553176","imdata":[]
#
########################################################################################################################
def get_fabric_nodes():
    url = F"https://{apicIP}/api/node/class/fabricNode.json"
    response = json.loads(requests.request("GET", url, headers=headers, verify=False).text)
    print(response)
    return

########################################################################################################################

########################################################################################################################
def get_backup_count():
    url = F'https://{apicIP}/api/node/class/configSnapshot.json?query-target-filter=and(not(wcard(configSnapshot.dn,"__ui_")),wcard(configSnapshot.rootDn,"^(?!uni/tn-).*"))'
    response = json.loads(requests.request("GET", url, headers=headers, verify=False).text)
    count = int(response["totalCount"])
    return count
########################################################################################################################


########################################################################################################################
def create_backup():
    count_start = get_backup_count()
    url = F'https://{apicIP}/api/node/mo/uni/fabric/configexp-defaultOneTime.json'
    payload = '{"configExportP":{"attributes":{"dn":"uni/fabric/configexp-defaultOneTime","name":"defaultOneTime","snapshot":"true","targetDn":"","adminSt":"triggered","rn":"configexp-defaultOneTime","status":"created,modified","descr":"Python Backup"},"children":[]}}'
    print("creating backup")
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    for itter in range(5):
        print("waiting for backup to finish")
        time.sleep(10)
        count_end = get_backup_count()
        if count_start == count_end and itter == 4:
            print("Error: Backup timed out!")
            print("")
            sys.exit()
        elif count_start != count_end:
            break

    return response
########################################################################################################################



########################################################################################################################
def add_switch_to_fabric(serial_number, node_id, switch_name, role):
    # Add switch
    url = F"https://{apicIP}/api/node/mo/uni/controller/nodeidentpol/nodep-{serial_number}.json"
    payload = F'{{"fabricNodeIdentP":{{"attributes":{{"dn":"uni/controller/nodeidentpol/nodep-{serial_number}","serial":"{serial_number}","nodeId":"{node_id}","name":"{switch_name}","role":"{role}","rn":"nodep-{serial_number}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return response


########################################################################################################################


########################################################################################################################
def register_switch_on_fabric(serial_number, switch_name, node_id):
    url = F"https://{apicIP}/api/node/mo/uni/controller.json"
    payload = F'{{"fabricNodeIdentP":{{"serial": "{serial_number}","name": "{switch_name}","nodeId": "{node_id}"}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return response


########################################################################################################################


########################################################################################################################
# get epg on physical interface
def get_epgs_on_physport(pod, leaf, interface):
    EPGs = []
    url2 = F"https://{apicIP}/api/node/mo/topology/pod-{pod}/node-{leaf}/sys/phys-[eth{interface}].json?rsp-subtree-include=full-deployment&target-node=all&target-path=l1EthIfToEPg"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    EPGs_json = json.loads(response2.text)["imdata"][0]["l1PhysIf"]["children"][0]["pconsCtrlrDeployCtx"]
    count = EPGs_json["attributes"]["count"]
    EPGs_list = EPGs_json["children"]
    for i in range(int(count)):
        EPGs.append(EPGs_list[i]["pconsResourceCtx"]["attributes"]["ctxDn"].split("/")[3][4:])
    return EPGs


########################################################################################################################

########################################################################################################################
# get physical interface on VPCs
def get_physport_on_vpc(tenant, ap, epg, pod, leaf, vpc, node):
    physport = []
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant}/ap-{ap}/epg-{epg}/rspathAtt-[topology/pod-{pod}/protpaths-{leaf}/pathep-[{vpc}]].json?rsp-subtree-include=full-deployment&target-node={node}&target-path=EPgToNwIf"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    physport_list = json.loads(response2.text)["imdata"][0]["fvAEPg"]["children"][2]["pconsNodeDeployCtx"]["children"]
    for i in range(len(physport_list)):
        physport.append(physport_list[i]["pconsResourceCtx"]["attributes"]["ctxDn"].split("-")[3][1:-1])
    return physport


########################################################################################################################


########################################################################################################################
# modify deployment immediacy for vpc. Input mode as 'immediate' for immediate and 'lazy' for on demand
def modify_vpc_deployment(mode, tenant_name, apProfile, EPGname, pod, leaf, vpc):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rspathAtt-[topology/pod-{pod}/protpaths-{leaf}/pathep-[{vpc}]].json"
    payload = F'{{"fvRsPathAtt":{{"attributes":{{"instrImedcy":"{mode}","tDn":"topology/pod-{pod}/protpaths-{leaf}/pathep-[{vpc}]"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# modify deployment immediacy for port. Input mode as 'immediate' for immediate and 'lazy' for on demand
def modify_port_deployment(mode, tenant_name, apProfile, EPGname, pod, leaf, port):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rspathAtt-[topology/pod-{pod}/paths-{leaf}/pathep-[{port}]].json"
    payload = F'{{"fvRsPathAtt":{{"attributes":{{"instrImedcy":"{mode}","tDn":"topology/pod-{pod}/paths-{leaf}/pathep-[{port}]"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Delete leaf access port policy
def delLeafaccport(policy_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/infra/funcprof/accportgrp-{policy_name}.json"
    payload = F'{{"infraAccPortGrp":{{"attributes":{{"dn":"uni/infra/funcprof/accportgrp-{policy_name}","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Delete PC/vpc group policy
def delVPCpol(policy_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/infra/funcprof/accbundle-{policy_name}.json"
    payload = F'{{"infraAccBndlGrp":{{"attributes":{{"dn":"uni/infra/funcprof/accbundle-{policy_name}","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Deassociate physical port from epg
def DeassociatePort(tenant_name, apProfile, EPGname, pod, leaf, port):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rspathAtt-[topology/pod-{pod}/paths-{leaf}/pathep-[eth1/{port}]].json"
    payload = F'{{"fvRsPathAtt":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rspathAtt-[topology/pod-{pod}/paths-{leaf}/pathep-[eth1/{port}]]","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function that Deassociates vpc. Requires AP, epg,pod, leaf and vpc
def DeassociateVPC(tenant_name, apProfile, EPGname, pod, leaf, vpc):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rspathAtt-[topology/pod-{pod}/protpaths-{leaf}/pathep-[{vpc}]].json"
    payload = F'{{"fvRsPathAtt":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rspathAtt-[topology/pod-{pod}/protpaths-{leaf}/pathep-[{vpc}]]","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Deassociate physical domain from epg
def DeassociatePHY(tenant_name, apProfile, EPGname, Phy_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rsdomAtt-[uni/phys-{Phy_name}].json"
    payload = F'{{"fvRsDomAtt":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/rsdomAtt-[uni/phys-{Phy_name}]","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function to get SVI
def getSVI(tenant_name, L3out_name, NodeProf_name, LogicalInt_name):
    url2 = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{L3out_name}/lnodep-{NodeProf_name}/lifp-{LogicalInt_name}.json?query-target=subtree&target-subtree-class=l3extRsPathL3OutAtt&query-target-filter=not(wcard(l3extRsPathL3OutAtt.dn,%22__ui_%22))&target-subtree-class=l3extRsPathL3OutAtt,l3extIp,l3extMember'
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    Svilist_json = json.loads(response2.text)
    SVI_list = ''
    if int(Svilist_json["totalCount"]) != 2:
        SVI_list = []
        IP_sideA = []
        IP_sideB = []
        j = 0
        for i in range(int(int(Svilist_json["totalCount"]) / 5),
                       int((int(Svilist_json["totalCount"]) + int(int(Svilist_json["totalCount"]) / 5)) / 2)):
            line = re.split('/', Svilist_json["imdata"][i]["l3extMember"]["attributes"]["dn"])
            line_pod = line[6].split("pod-")[1]
            line_protpath = line[7].split("protpaths-")[1]
            line_vpc = line[8].split("[")[1].split("]")[0]
            side = Svilist_json["imdata"][i]["l3extMember"]["attributes"]["side"]
            IP_secondary = \
                Svilist_json["imdata"][i + 2 * (int(int(Svilist_json["totalCount"]) / 5))]["l3extIp"]["attributes"][
                    "addr"]
            if side == 'A':
                IP_sideA = Svilist_json["imdata"][i]["l3extMember"]["attributes"]["addr"]
            elif side == 'B':
                IP_sideB = Svilist_json["imdata"][i]["l3extMember"]["attributes"]["addr"]
            if str(line_vpc) not in str(SVI_list) and int(
                    (i + (int(int(Svilist_json["totalCount"]) / 5) - 1)) % 2) == 0:
                line_vlan = Svilist_json["imdata"][j]["l3extRsPathL3OutAtt"]["attributes"]["encap"].split("vlan-")[1]
                j += 1
                SVI_list.append([line_pod, line_protpath, line_vpc, IP_sideA, IP_sideB, IP_secondary, line_vlan])
    return SVI_list


########################################################################################################################

########################################################################################################################
def createSVI(tenant_name, L3out_name, NodeProf_name, LogicalInt_name, pod, node, vpc, vlan, IP_sideA, IP_sideB,
              IP_secondary):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{L3out_name}/lnodep-{NodeProf_name}/lifp-{LogicalInt_name}/rspathL3OutAtt-[topology/pod-{pod}/protpaths-{node}/pathep-[{vpc}]].json"
    payload = F'{{"l3extRsPathL3OutAtt":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{L3out_name}/lnodep-{NodeProf_name}/lifp-{LogicalInt_name}/rspathL3OutAtt-[topology/pod-{pod}/protpaths-{node}/pathep-[{vpc}]]","mac":"00:22:BD:F8:19:FF","ifInstT":"ext-svi","encap":"vlan-{vlan}","tDn":"topology/pod-{pod}/protpaths-{node}/pathep-[{vpc}]","rn":"rspathL3OutAtt-[topology/pod-{pod}/protpaths-{node}/pathep-[{vpc}]]","status":"created"}},"children":[{{"l3extMember":{{"attributes":{{"addr":"{IP_sideA}","status":"created","side":"A"}},"children":[{{"l3extIp":{{"attributes":{{"addr":"{IP_secondary}","status":"created"}},"children":[]}}}}]}}}},{{"l3extMember":{{"attributes":{{"side":"B","addr":"{IP_sideB}","status":"created"}},"children":[{{"l3extIp":{{"attributes":{{"addr":"{IP_secondary}","status":"created"}},"children":[]}}}}]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
def DelSVI(tenant_name, L3out_name, NodeProf_name, LogicalInt_name, pod, node, vpc):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{L3out_name}/lnodep-{NodeProf_name}/lifp-{LogicalInt_name}/rspathL3OutAtt-[topology/pod-{pod}/protpaths-{node}/pathep-[{vpc}]].json"
    payload2 = F'{{"l3extRsPathL3OutAtt":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{L3out_name}/lnodep-{NodeProf_name}/lifp-{LogicalInt_name}/rspathL3OutAtt-[topology/pod-{pod}/protpaths-{node}/pathep-[{vpc}]]","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload2, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
def getL3(tenant_name):
    url2 = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}.json?query-target=children&target-subtree-class=l3extOut&query-target-filter=not(wcard(l3extOut.dn,%22__ui_%22))&&page=0&page-size=40'
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    L3_line = json.loads(response2.text)
    L3_name = []
    if int(L3_line["totalCount"]) != 0:
        for i in range(int(L3_line["totalCount"])):
            L3_name.append(L3_line["imdata"][i]["l3extOut"]["attributes"]["name"])
    return L3_name


########################################################################################################################

########################################################################################################################
def getNodeProf(tenant_name, L3out_name):
    url2 = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{L3out_name}.json?query-target=children&target-subtree-class=l3extLNodeP&query-target-filter=not(wcard(l3extLNodeP.dn,%22__ui_%22))'
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    NodeProf_line = json.loads(response2.text)
    NodeProf_name = []
    if int(NodeProf_line["totalCount"]) != 0:
        for i in range(int(NodeProf_line["totalCount"])):
            NodeProf_name.append(NodeProf_line["imdata"][i]["l3extLNodeP"]["attributes"]["name"])
    return NodeProf_name


########################################################################################################################

########################################################################################################################
def getLogicalInt(tenant_name, L3out_name, NodeProf_name):
    url2 = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{L3out_name}/lnodep-{NodeProf_name}.json?query-target=children&target-subtree-class=l3extLIfP&query-target-filter=not(wcard(l3extLIfP.dn,%22__ui_%22))'
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    LogicalInt_line = json.loads(response2.text)
    LogicalInt_name = []
    if int(LogicalInt_line["totalCount"]) != 0:
        for i in range(int(LogicalInt_line["totalCount"])):
            LogicalInt_name.append(LogicalInt_line["imdata"][0]["l3extLIfP"]["attributes"]["name"])
    return LogicalInt_name


########################################################################################################################


########################################################################################################################
# Authentication Function: returns headers with authentication cookie inside. Only valid for a few minutes.
def refreshAuth():
    url = F"https://{apicIP}/api/mo/aaaLogin.json"
    payload = F'{{"aaaUser":{{"attributes":{{"name":"{username}","pwd":"{password}"}}}}}}'
    headers2 = {'Content-Type': "application/json", 'Cache-Control': "no-cache", }
    response = requests.request("POST", url, data=payload, headers=headers2, verify=False)
    presponse = json.loads(response.text)
    ApicCookie = [presponse['imdata']][0][0]['aaaLogin']['attributes']['token']
    # Common header to be used for all functions
    global headers
    headers = {'Content-Type': "application/json", 'Accept': "application/jsons", 'cookie': F"APIC-cookie={ApicCookie}",
               'Cache-Control': "no-cache", }
    return headers


########################################################################################################################


refreshAuth()


########################################################################################################################
def GetPortsonEPGs(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?query-target=children&target-subtree-class=fvRsPathAtt"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    StaticPortsListJson = json.loads(response2.text)
    alist = []
    for i in range(len(StaticPortsListJson["imdata"])):
        path = re.split('/',re.search('\[(.*)\]', StaticPortsListJson["imdata"][i]["fvRsPathAtt"]["attributes"]["dn"]).group(1))
        vlan = StaticPortsListJson["imdata"][i]["fvRsPathAtt"]["attributes"]["encap"]
        try:
            a = re.sub("]", "", path[3]).partition('[')[2]
            if 'eth' in a:
                a = re.split(']', path[4])[0]
                alist.append(([path[1].partition("-")[2], path[2].partition("-")[2], a, vlan.partition("-")[2]]))
            else:
                alist.append(([path[1].partition("-")[2], path[2].partition("-")[2], a, vlan.partition("-")[2]]))
        except:
            pass  # print(path[2].partition("-")[2],re.sub("]", "", path[3]))
    return alist  # , StaticPortsListJson4


########################################################################################################################


########################################################################################################################
# Function fetches list of tenants available within APIC reach on the Fabric
def GetTennantList():
    url2 = F"https://{apicIP}/api/class/fvTenant.json"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    TennantList = json.loads(response2.text)
    alist = []
    for i in range(len(TennantList["imdata"])):
        alist.append(TennantList["imdata"][i]["fvTenant"]["attributes"]["name"])
    return alist


# return TennantList
########################################################################################################################
# print(GetTennantList())

########################################################################################################################
# Function fetches list of APs for given tenant
def GetAPList(tenant_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}.json?query-target=children&target-subtree-class=fvAp"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    APListJson = json.loads(response2.text)
    alist = []
    for i in range(len(APListJson["imdata"])):
        alist.append(APListJson["imdata"][i]["fvAp"]["attributes"]["name"])
    return alist


########################################################################################################################


########################################################################################################################
# Function fetches list of EPGs for given tenant and application PRofile
def GetEPGList(tenant_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}.json?query-target=subtree&target-subtree-class=fvAEPg&target-subtree-class=fvAEPg=fvBD"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    APListJson = json.loads(response2.text)
    alist = []
    for i in range(len(APListJson["imdata"])):
        alist.append(APListJson["imdata"][i]["fvAEPg"]["attributes"]["name"])
    # return APListJson
    return alist


########################################################################################################################


########################################################################################################################
# Function fetches list of associated EPGs for given BD
def GetEPG4BDList(tenant_name, BDname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/BD-{BDname}.json?query-target=children&target-subtree-class=relnFrom"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    EPGListJson = json.loads(response2.text)
    alist = []
    for i in range(len(EPGListJson["imdata"])):
        alist.append(re.split("-", re.split("/", EPGListJson["imdata"][i]["fvRtBd"]["attributes"]["tDn"])[-1])[-1])

    return alist


########################################################################################################################


########################################################################################################################
# Function fetches list of associated BD for given epg
def GetPhy4EPG(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?query-target=children&target-subtree-class=fvRsDomAtt"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    EPGListJson = json.loads(response2.text)
    alist = []
    for i in range(len(EPGListJson["imdata"])):
        alist.append((EPGListJson["imdata"][i]["fvRsDomAtt"]["attributes"]["tDn"]).partition("phys-")[2])

    # return EPGListJson
    return alist  # , EPGListJson


########################################################################################################################

########################################################################################################################
# Function fetches list of associated VMM domains for given epg
def GetVMMDomainsinEPGs(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?query-target=children&target-subtree-class=fvRsDomAtt"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    DomainsListJson = json.loads(response2.text)
    alist = []
    for i in range(len(DomainsListJson["imdata"])):
        try:
            alist.append(
                DomainsListJson["imdata"][i]["fvRsDomAtt"]["attributes"]["tDn"].partition("vmmp-")[2].partition("dom-")[
                    2])
        except:
            pass
    # remove empty elements from list
    alist = list(filter(None, alist))
    return alist


# return DomainsListJson
########################################################################################################################
# print(GetVMMDomainsinEPGs("tenant01","AP1","EPG1"))


########################################################################################################################
# Function fetches list of associated BD for given epg
def GetEPGObject(tenant_name, apProfile, EPGname):
    # URL to GET epg primary object
    url1 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json"
    # URL to GET epg children objects
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?&query-target=children"
    # Save response from both GET requests
    response1 = requests.request("GET", url1, headers=headers, verify=False)
    EPGListJson = json.loads(response1.text)['imdata'][0]
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    EPGListJson2 = json.loads(response2.text)
    # Add children objects to primary object
    EPGListJson["children"] = EPGListJson2['imdata']

    return EPGListJson


########################################################################################################################


########################################################################################################################
# Function fetches list of associated BD for given VRF
def GetBD4VRF(tenant_name, vrf):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ctx-{vrf}.json?query-target=children&target-subtree-class=relnFrom"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    EPGListJson = json.loads(response2.text)
    alist = []
    for i in range(len(EPGListJson["imdata"])):
        try:
            alist.append(EPGListJson["imdata"][i]["fvRtCtx"]["attributes"]["tDn"].partition("BD-")[2])
        except:
            pass
    return alist  # ,EPGListJson


########################################################################################################################
# print GetBD4VRF("tenant-1","tenant1-vrf1")

########################################################################################################################
# Function fetches list of VRFs for given tenant
def GetvrfList(tenant_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}.json?query-target=children&target-subtree-class=fvCtx"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    vrfListJson = json.loads(response2.text)
    alist = []
    for i in range(len(vrfListJson["imdata"])):
        alist.append(vrfListJson["imdata"][i]["fvCtx"]["attributes"]["name"])
    return alist


########################################################################################################################

########################################################################################################################
# Function fetches list of VRFs for given tenant
def GetBDList(tenant_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}.json?query-target=children&target-subtree-class=fvBD"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    BDListJson = json.loads(response2.text)
    alist = []
    for i in range(len(BDListJson["imdata"])):
        alist.append(BDListJson["imdata"][i]["fvBD"]["attributes"]["name"])

    return alist


########################################################################################################################

########################################################################################################################
# Function fetches list of static ports for given EPGs
def GetStaticPortsonEPGs(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?query-target=children&target-subtree-class=fvRsPathAtt"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    StaticPortsListJson = json.loads(response2.text)
    alist = []
    for i in range(len(StaticPortsListJson["imdata"])):
        path = re.split('/',
                        re.search('[(.*)]', StaticPortsListJson["imdata"][i]["fvRsPathAtt"]["attributes"]["dn"]).group(
                            1))
        vlan = StaticPortsListJson["imdata"][i]["fvRsPathAtt"]["attributes"]["encap"]
        try:
            alist.append(([path[1].partition("-")[2], path[2].partition("-")[2], re.sub("]", "", path[4]),
                           vlan.partition("-")[2]]))
        except:
            pass
    return alist  # , StaticPortsListJson4


########################################################################################################################

########################################################################################################################
# Function fetches list of static ports for given EPGs
def GetBD4EPG(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?query-target=children"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    StaticPortsListJson4 = json.loads(response2.text)['imdata'][0]['fvRsBd']['attributes']['tnFvBDName']
    return StaticPortsListJson4


########################################################################################################################


########################################################################################################################
# Function fetches list of static ports for given EPGs
def GetEPGinfo(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?query-target=children"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    StaticPortsListJson = json.loads(response2.text)
    return StaticPortsListJson[0]


########################################################################################################################

########################################################################################################################
# Function fetches list of static ports for given EPGs
def GetVLP():
    url2 = F"https://{apicIP}/api/node/mo/uni/infra.json?query-target=children&query-target=subtree&target-subtree-class=fvnsVlanInstP&query-target=children&target-subtree-class=fvnsEncapBlk"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    StaticPortsListJson = json.loads(response2.text)
    return StaticPortsListJson


########################################################################################################################

########################################################################################################################
# Function fetches list of static ports for given EPGs
def GetVPC():
    url2 = F"https://{apicIP}/api/node/class/infraAccBndlGrp.json"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    VPCListJson = json.loads(response2.text)
    alist = []
    for i in range(len(VPCListJson["imdata"])):
        alist.append(VPCListJson["imdata"][i]["infraAccBndlGrp"]["attributes"]["name"])
    return alist


########################################################################################################################

########################################################################################################################
# Function adds Tenant to the logical topology. The argument needs to be the name of Tenant
def AddTennant(tenant_name):
    url2 = F"https://{apicIP}/api/mo/uni.json"
    payload = F'{{"fvTenant" : {{"attributes" : {{"name" : "{tenant_name}" }}}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    TennantList = json.loads(response2.text)
    return TennantList


########################################################################################################################

########################################################################################################################
# Function adds BD to the logical topology. The argument needs to be the name of BD, tenant
def AddBD(BDname, tenant_name, **kwargs):  # vrf is same as Context
    """Takes 2 compulsory arguments.
	Optional arguments parse in
	unicaseRoute = 'true' or 'false' as optional argument to set as L3 or L2. Same for
	L2 unknown multicast flooding L2UnknownUnicast = 'flood' or 'proxy'
	arpFlood = yes or no
	"""
    url2 = F"https://{apicIP}/api/mo/uni/tn-{tenant_name}.json"
    # if kwargs['unicastRoute']: unicastRoute = kwargs['unicastRoute'] else: unicastRoute = 'true'
    unicastRoute = kwargs['unicastRoute'] if 'unicastRoute' in kwargs else 'true'
    unkMacUcastAct = kwargs['L2UnknownUnicast'] if 'L2UnknownUnicast' in kwargs else 'proxy'
    arpFlood = kwargs['arpFlood'] if 'arpFlood' in kwargs else 'no'
    payload = F'{{"fvBD":{{"attributes":{{"dn":"uni/tn-{tenant_name}/BD-{BDname}","name":"{BDname}","arpFlood":"{arpFlood}","unicastRoute":"{unicastRoute}","unkMacUcastAct":"{unkMacUcastAct}","rn":"BD-{BDname}","status":"created,modified"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function associates VRF to specified BD.
def AssociateVRFToBD(tenant_name, BDname, vrf):  # vrf is same as Context
    url2 = F"https://{apicIP}/api/mo/uni/tn-{tenant_name}/BD-{BDname}/rsctx.json"
    payload = F'{{"fvRsCtx":{{"attributes":{{"tnFvCtxName":"{vrf}"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function adds subnet to BD
def AddSubnetToBD(tenant_name, BDname, subnet, scope):  # vrf is same as Context
    url2 = F"https://{apicIP}/api/mo/uni/tn-{tenant_name}/BD-{BDname}.json"
    payload = F'{{"fvBD":{{"attributes":{{"dn":"uni/tn-{tenant_name}/BD-{BDname}","status":"modified"}},"children":[{{"fvSubnet":{{"attributes":{{"dn":"uni/tn-{tenant_name}/BD-{BDname}/subnet-[{subnet}]","status":"created","scope":"{scope}"}},"children":[]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function adds subnet to epg
def AddSubnetToEPG(tenant_name, apProfile, EPGname, subnet, scope):
    url2 = F"https://{apicIP}/api/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}/subnet-[{subnet}].json"
    payload = F'{{"fvSubnet":{{"attributes":{{"dn":"uni/tn-[{tenant_name}]/ap-{apProfile}/epg-{EPGname}/subnet-[{subnet}]","ctrl":"unspecified","ip":"{subnet}","scope":"{scope}","rn":"subnet-[{subnet}]","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function adds VRF to the logical topology under a tenant
def AddVRF2Tenant(tenant_name, vrf):  # vrf is same as Context
    url2 = F"https://{apicIP}/api/mo/uni/tn-{tenant_name}/ctx-{vrf}.json"
    payload = F'{{"fvCtx":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ctx-{vrf}","name":"{vrf}","rn":"ctx-{vrf}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function adds epg to the logical topology. The argument needs name of epg and corresponding tenant_name, apProfile, BridgeDomain
def AddEPG(tenant_name, apProfile, BDname, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}.json"
    payload = F'{{"fvAEPg":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}","name":"{EPGname}","rn":"epg-{EPGname}","status":"created"}},"children":[{{"fvRsBd":{{"attributes":{{"tnFvBDName":"{BDname}","status":"created,modified"}},"children":[]}}}}]}}}},{{"fvRsApMonPol":{{"attributes":{{"tnMonEPGPolName":"default","status":"created,modified"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function deletes epg to the logical topology. The argument needs name of epg and corresponding tenant_name, apProfile, BridgeDomain
def DelEPG(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}.json"
    payload = F'{{"fvAEPg":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}","name":"{EPGname}","rn":"epg-{EPGname}","status":"deleted"}}}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function adds epg to the logical topology. The argument needs name of epg and corresponding tenant_name, apProfile, BridgeDomain
def postunderAP(tenant_name, apProfile, payload):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}.json"
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function adds epg to the logical topology. The argument needs name of epg and corresponding tenant_name, apProfile, BridgeDomain
def AddAP(tenant_name, apProfile):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}.json"
    payload = F'{{"fvAp":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}","name":"{apProfile}","rn":"ap-{apProfile}","status":"created"}}}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function associates a Physical Domain (Fabric Access Policy) to epg
def AssociatePhyDom2EPG(tenant_name, apProfile, EPGname, PHY):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json"
    payload = F'{{"fvRsDomAtt":{{"attributes":{{"resImedcy":"immediate","tDn":"uni/phys-{PHY}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function associates a BridgeDomain to epg
def AssociateBD2EPG(tenant_name, apProfile, EPGname, BDname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json"
    payload = F'{{"fvRsBd":{{"attributes":{{"tnFvBDName":"{BDname}","status":"created,modified"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function associates a Physical Domain (Fabric Access Policy) to epg
def AssociateVMDom2EPG(tenant_name, apProfile, EPGname, VMDom):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json"
    payload = F'{{"fvRsDomAtt":{{"attributes":{{"instrImedcy":"immediate","resImedcy":"pre-provision","tDn":"uni/vmmp-VMware/dom-{VMDom}","status":"created,modified"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function adds Static Port association to epg
def AssociateStaticPort2EPG(tenant_name, apProfile, EPGname, Vlan, pod, leafpath, port, **kwargs):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json"
    if kwargs["mode"] == 'Untagged':
        mode = ',"mode":"untagged",'
    elif kwargs["mode"] == 'Native':
        mode = ',"mode":"native",'
    else:
        mode = ''
    payload = F'{{"fvRsPathAtt":{{"attributes":{{"encap":"vlan-{Vlan}"{mode}"tDn":"topology/pod-{pod}/paths-{leafpath}/pathep-[eth1/{port}]","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function adds Static Port association to epg
def AssociateVPCPort2EPG(tenant_name, apProfile, EPGname, Vlan, pod, leafpath, port, **kwargs):
    if kwargs["mode"] == 'Untagged':
        mode = '"mode":"untagged",'
    elif kwargs["mode"] == 'Native':
        mode = '"mode":"native",'
    else:
        mode = ''
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json"
    payload = F'{{"fvRsPathAtt":{{"attributes":{{{mode}"instrImedcy":"lazy","encap":"vlan-{Vlan}","tDn":"topology/pod-{pod}/protpaths-{leafpath}/pathep-[{port}]","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function adds VLAN pool to Fabric. Arguments needed: VLP name, range from, range to, allocation mode (dynamic/static).
def addVLP(VLP_name, allocationMode, range_start, range_end):
    url2 = F"https://{apicIP}/api/node/mo/uni/infra/vlanns-[{VLP_name}]-{allocationMode}.json"
    payload = F'{{"fvnsVlanInstP":{{"attributes":{{"dn":"uni/infra/vlanns-[{VLP_name}]-{allocationMode}","name":"{VLP_name}","allocMode":"{allocationMode}","rn":"vlanns-[{VLP_name}]-{allocationMode}","status":"created"}},"children":[{{"fvnsEncapBlk":{{"attributes":{{"dn":"uni/infra/vlanns-[{VLP_name}]-{allocationMode}/from-[vlan-{range_start}]-to-[vlan-{range_end}]","from":"vlan-{range_start}","to":"vlan-{range_end}","rn":"from-[vlan-{range_start}]-to-[vlan-{range_end}]","status":"created"}},"children":[]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function creates and attaches AEP to specified PhysicalDomain. Name of AEP and Phys domain name needed for argument
def attachAEP(AEP_name, Phy_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/infra/attentp-{AEP_name}.json"
    payload = F'{{"infraRsDomP":{{"attributes":{{"tDn":"uni/phys-{Phy_name}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
# Function creates AEP. Name of AEP needed for argument
def createAEP(AEP_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/infra.json"
    payload = F'{{"infraInfra":{{"attributes":{{"dn":"uni/infra","status":"modified"}},"children":[{{"infraAttEntityP":{{"attributes":{{"dn":"uni/infra/attentp-{AEP_name}","name":"{AEP_name}","rn":"attentp-{AEP_name}","status":"created"}},"children":[]}}}},{{"infraFuncP":{{"attributes":{{"dn":"uni/infra/funcprof","status":"modified"}}}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function creates PhysicalDomain and associates with VLP. Name of VLP and Phys domain name needed for argument
def createPHY(Phy_name):
    url = F"https://{apicIP}/api/node/mo/uni/phys-{Phy_name}.json"
    payload = F'{{"physDomP":{{"attributes":{{"dn":"uni/phys-{Phy_name}","name":"{Phy_name}","rn":"phys-{Phy_name}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return response.text


########################################################################################################################


########################################################################################################################
def attach_vlp_to_phy(Phy_name, VLP_name, allocation_mode):
    url = F"https://{apicIP}/api/node/mo/uni/phys-{Phy_name}/rsvlanNs.json"
    payload = F'{{"infraRsVlanNs":{{"attributes":{{"tDn":"uni/infra/vlanns-[{VLP_name}]-{allocation_mode}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return response.text


########################################################################################################################


########################################################################################################################
def createVPCPolGrp(VPCPolGrpName, AEP, **kwargs):
    """Creates a VPCPolGrp using defaults for CDP, LLDP, and LACP.
	2 Compulsory arguments in this order: vpc Policy Group name, AEP
	Optional keyword arguments: CDP = "", LLDP= "", LACP = ""
	If you want to change these to different policies, then please parse in
	the name of the policies as optional arguments. For e.g.
	createVPCPolGrp("myVPCPolGrp","myAEP", CDP="CDP_enable", LLDP="LLDP_enable", LACP="LACP_Active")

	Please note that the policies will have to be preconfigured (i.e. LLDP_enable must be a policy) for the optional
	arguments to work.
	You don't have to use all optional arguments."""

    url = F'https://{apicIP}/api/node/mo/uni/infra/funcprof.json'
    payload = '{"infraFuncP":{"attributes":{"dn":"uni/infra/funcprof","status":"modified"},"children":[]}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)

    url2 = F"https://{apicIP}/api/node/mo/uni/infra/funcprof/accbundle-{VPCPolGrpName}.json"
    LLDP = kwargs['LLDP'] if 'LLDP' in kwargs else "default"
    CDP = kwargs['CDP'] if 'CDP' in kwargs else 'default'
    LACP = kwargs['LACP'] if 'LACP' in kwargs else 'LACP_Active'
    payload2 = F'{{"infraAccBndlGrp":{{"attributes":{{"dn":"uni/infra/funcprof/accbundle-{VPCPolGrpName}","lagT":"node","name":"{VPCPolGrpName}","rn":"accbundle-{VPCPolGrpName}","status":"created"}},"children":[{{"infraRsAttEntP":{{"attributes":{{"tDn":"uni/infra/attentp-{AEP}","status":"created"}},"children":[]}}}},{{"infraRsLacpPol":{{"attributes":{{"tnLacpLagPolName":"{LACP}","status":"created"}},"children":[]}}}},{{"infraRsCdpIfPol":{{"attributes":{{"tnCdpIfPolName":"{CDP}","status":"created"}},"children":[]}}}},{{"infraRsLldpIfPol":{{"attributes":{{"tnLldpIfPolName":"{LLDP}","status":"created"}},"children":[]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload2, headers=headers, verify=False)
    return json.loads(response2.text)


def createIntPolGrp(IntPolGrpName, AEP_name, **kwargs):
    """Creates a interface PolGrp using defaults for CDP, LLDP
	2 Compulsory arguments in this order: vpc Policy Group name, AEP
	Optional keyword arguments: CDP = "", LLDP= ""
	If you want to change these to different policies, then please parse in
	the name of the policies as optional arguments. For e.g.
	createVPCPolGrp("myVPCPolGrp","myAEP", CDP="CDP_enable", LLDP="LLDP_enable")

	Please note that the policies will have to be preconfigured (i.e. LLDP_enable must be a policy) for the optional
	arguments to work.
	You don't have to use all optional arguments."""

    url2 = F"https://{apicIP}/api/node/mo/uni/infra/funcprof/accportgrp-{IntPolGrpName}.json"

    LLDP = kwargs['LLDP'] if 'LLDP' in kwargs else "default"
    CDP = kwargs['CDP'] if 'CDP' in kwargs else 'default'

    payload = F'{{"infraAccPortGrp":{{"attributes":{{"dn":"uni/infra/funcprof/accportgrp-{IntPolGrpName}","name":"{IntPolGrpName}","rn":"accportgrp-{IntPolGrpName}","status":"created"}},"children":[{{"infraRsAttEntP":{{"attributes":{{"tDn":"uni/infra/attentp-{AEP_name}","status":"created"}},"children":[]}}}},{{"infraRsCdpIfPol":{{"attributes":{{"tnCdpIfPolName":"{CDP}","status":"created"}},"children":[]}}}},{{"infraRsLldpIfPol":{{"attributes":{{"tnLldpIfPolName":"{LLDP}","status":"created"}},"children":[]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


def createIntProf(IntProfName):
    """Creates a IntProfName. Only argument taken in is the name of the interface Profile.
	"""

    url2 = F"https://{apicIP}/api/node/mo/uni/infra/accportprof-{IntProfName}.json"
    payload = F'{{"infraAccPortP":{{"attributes":{{"dn":"uni/infra/accportprof-{IntProfName}","name":"{IntProfName}","rn":"accportprof-{IntProfName}","status":"created"}},"children":[]}}}}'

    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


def add_vpc_ptsel_to_int_prof(IntProfName, PortSelectorName, Port, PolGrp):
    """Adds Ports to InterfaceProfile. Compulsory arguments in following order:
	IntProfName: This profile must exist already
	PortSelectorName: Choose a name for the new port selector you want to create
	Port: Which port do you want to associate? Just the number. Do not use 1/27, use 27.
	"""

    url2 = F"https://{apicIP}/api/node/mo/uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range.json"

    payload = F'{{"infraHPortS":{{"attributes":{{"dn":"uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range","name":"{PortSelectorName}","rn":"hports-{PortSelectorName}-typ-range","status":"created"}},"children":[{{"infraPortBlk":{{"attributes":{{"dn":"uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range/portblk-block2","fromPort":"{Port}","toPort":"{Port}","name":"block2","rn":"portblk-block2","status":"created"}},"children":[]}}}},{{"infraRsAccBaseGrp":{{"attributes":{{"tDn":"uni/infra/funcprof/accbundle-{PolGrp}","status":"created"}},"children":[]}}}}]}}}}'

    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


def add_interface_ptsel_to_int_prof(IntProfName, PortSelectorName, Port, PolGrp):
    """Adds Ports to InterfaceProfile. Compulsory arguments in following order:
    IntProfName: This profile must exist already
    PortSelectorName: Choose a name for the new port selector you want to create
    Port: Which port do you want to associate? Just the number. Do not use 1/27, use 27.
    """

    url2 = F"https://{apicIP}/api/node/mo/uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range.json"

    payload = F'{{"infraHPortS":{{"attributes":{{"dn":"uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range","name":"{PortSelectorName}","rn":"hports-{PortSelectorName}-typ-range","status":"created"}},"children":[{{"infraPortBlk":{{"attributes":{{"dn":"uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range/portblk-block2","fromPort":"{Port}","toPort":"{Port}","name":"block2","rn":"portblk-block2","status":"created"}},"children":[]}}}},{{"infraRsAccBaseGrp":{{"attributes":{{"tDn":"uni/infra/funcprof/accportgrp-{PolGrp}","status":"created"}},"children":[]}}}}]}}}}'

    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


def addVPCPolGrptoPortSel(IntProfName, PortSelectorName, PolGrp):
    """Adds vpc Policy Group to a port selector under an Interface profile. Important for binding 2+ interfaces into vpc
	"""
    url2 = F"https://{apicIP}/api/node/mo/uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range.json"
    payload = F'{{"infraRsAccBaseGrp":{{"attributes":{{"tDn":"uni/infra/funcprof/accbundle-{PolGrp}"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


def DeletePortSeltoInt(IntProfName, PortSelectorName):
    """Deletes portselectors to a given Interface profile
	"""
    url2 = F"https://{apicIP}/api/node/mo/uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range.json"
    payload = F'{{"infraHPortS":{{"attributes":{{"dn":"uni/infra/accportprof-{IntProfName}/hports-{PortSelectorName}-typ-range","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


def createSwitchProf(SwitchProfName):
    """Creates a Switch Profile. Only argument taken in is the name of the Switch Profile.
	"""
    url2 = F"https://{apicIP}/api/node/mo/uni/infra//nprof-{SwitchProfName}.json"  # , apProfile,EPGname)
    payload = F'{{"infraNodeP":{{"attributes":{{"dn":"uni/infra/nprof-{SwitchProfName}","name":"{SwitchProfName}","rn":"nprof-{SwitchProfName}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


def AddLeafSeltoSwitchProf(SwitchProfName, SwitchSelectorName, Switch):
    """Adds switches to Switch Profile. Compulsory arguments in following order:
	SwitchProfName: This profile must exist already
	SwitchSelectorName: Choose a name for the new Switch selector you want to create
	Switch: Which port do you want to associate? Just the number. For e.g. 101
	Kwargs: if you want a switch based AEP. Functionality to be added later
	"""
    url2 = F"https://{apicIP}/api/node/mo/uni/infra/nprof-{SwitchProfName}/leaves-{SwitchSelectorName}-typ-range.json"
    payload = F'{{"infraLeafS":{{"attributes":{{"dn":"uni/infra/nprof-{SwitchProfName}/leaves-{SwitchSelectorName}-typ-range","type":"range","name":"{SwitchSelectorName}","status":"created","rn":"leaves-{SwitchSelectorName}-typ-range"}},"children":[{{"infraNodeBlk":{{"attributes":{{"dn":"uni/infra/nprof-{SwitchProfName}/leaves-{SwitchSelectorName}-typ-range/nodeblk-7bd8170e51d90b40","from_":"{Switch}","to_":"{Switch}","status":"created"}},"children":[]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################

########################################################################################################################
def add_inteface_selector_to_switch_profile(SwitchProfName, IntProfName):
    url = F'https://{apicIP}/api/node/mo/uni/infra/nprof-{SwitchProfName}.json'
    payload = F'{{"infraRsAccPortP":{{"attributes":{{"tDn":"uni/infra/accportprof-{IntProfName}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return response.text


########################################################################################################################

########################################################################################################################
def addIntProfToSwitchProf(SwitchProfName, IntProfName):
    """Adds an existing InterfaceProfile to a Switch Profile. Compulsory arguments in this order -> switch profile name, interface profile name
	"""
    url2 = F"https://{apicIP}/api/node/mo/uni/infra//nprof-{SwitchProfName}.json"
    payload = F'{{"infraRsAccPortP":{{"attributes":{{"tDn":"uni/infra/accportprof-{IntProfName}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
def createContractandAssociate(tenant_name, apProfile, provEPG, conEPG, contract_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}.json"
    payload = F'{{"fvTenant":{{"attributes":{{"dn":"uni/tn-{tenant_name}","status":"modified"}},"children":[{{"fvAp":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}","status":"modified"}},"children":[{{"fvAEPg":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}/epg-{provEPG}","status":"modified"}},"children":[{{"fvRsProv":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"created"}},"children":[]}}}}]}}}},{{"fvAEPg":{{"attributes":{{"dn":"uni/tn-{tenant_name}/ap-{apProfile}/epg-{conEPG}","status":"modified"}},"children":[{{"fvRsCons":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"created"}},"children":[]}}}}]}}}}]}}}},{{"vzBrCP":{{"attributes":{{"dn":"uni/tn-{tenant_name}/brc-{contract_name}","name":"{contract_name}","scope":"tenant","status":"created"}},"children":[{{"vzSubj":{{"attributes":{{"dn":"uni/tn-{tenant_name}/brc-{contract_name}/subj-Subject","name":"Subject","status":"created"}},"children":[{{"vzRsSubjFiltAtt":{{"attributes":{{"tnVzFilterName":"default","status":"created"}},"children":[]}}}}]}}}}]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
def GetContractList(tenant_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}.json?query-target=children&target-subtree-class=vzBrCP"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    listOfContracts = [v['vzBrCP']['attributes']['name'] for v in json.loads(response2.text)['imdata']]
    return listOfContracts


########################################################################################################################


########################################################################################################################
def GetContractList4EPGs(tenant_name, apProfile, EPGname):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{EPGname}.json?query-target=subtree&target-subtree-class=fvRsCons&target-subtree-class=fvRsProv"
    response2 = requests.request("GET", url2, headers=headers, verify=False)
    listOfProvidedContracts = [v['fvRsProv']['attributes']['tnVzBrCPName'] for v in json.loads(response2.text)['imdata']
                               if 'fvRsProv' in v]
    listOfConsumedContracts = [v['fvRsCons']['attributes']['tnVzBrCPName'] for v in json.loads(response2.text)['imdata']
                               if 'fvRsCons' in v]
    return listOfConsumedContracts, listOfProvidedContracts


########################################################################################################################


########################################################################################################################
def AssociateEPGasProvider(tenant_name, apProfile, epg, contract_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{epg}.json"
    payload = F'{{"fvRsProv":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
def AssociateEPGasConsumer(tenant_name, apProfile, epg, contract_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{epg}.json"
    payload = F'{{"fvRsCons":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
def DeleteEPGasProvider(tenant_name, apProfile, epg, contract_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{epg}.json"
    payload = F'{{"fvRsProv":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
def DeleteEPGasConsumer(tenant_name, apProfile, epg, contract_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{epg}.json"
    payload = F'{{"fvRsCons":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"deleted"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
def AssociateEPGasConsumertoExternal(tenant_name, apProfile, epg, contract_name):
    url2 = F"https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/ap-{apProfile}/epg-{epg}.json"
    payload = F'{{"fvRsConsIf":{{"attributes":{{"tnVzCPIfName":"{contract_name}","status":"created"}},"children":[]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
def addAdminUser(uid, firstname, lastname, pwd):
    """
	This function creates a new admin user with all priveleges. Please pass in the arguments in the following order:
	username,	firstname,	lastname,	password)
	They are all compusory.
	"""
    url2 = F"https://{apicIP}/api/node/mo/uni/userext/{uid}.json"
    payload = F'{{"aaaUser":{{"attributes":{{"dn":"uni/userext/user-{uid}","name":"{uid}","pwd":"{pwd}","firstName":"{firstname}","lastName":"{lastname}","accountStatus":"active","rn":"user-{uid}","status":"created"}},"children":[{{"aaaUserDomain":{{"attributes":{{"dn":"uni/userext/user-{uid}/userdomain-all","name":"all","status":"created,modified"}},"children":[{{"aaaUserRole":{{"attributes":{{"dn":"uni/userext/user-{uid}/userdomain-all/role-admin","name":"admin","privType":"writePriv","status":"created,modified"}},"children":[]}}}}]}}}}]}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    return json.loads(response2.text)


########################################################################################################################


########################################################################################################################
# Function adds Tenant to the logical topology. The argument needs to be the name of Tenant
def DeleteTennant(tenant_name):
    url2 = F"https://{apicIP}/api/mo/uni.json"
    payload = F'{{"fvTenant" : {{"attributes" : {{"name" : "{tenant_name}","status" : "deleted" }}}}}}'
    response2 = requests.request("POST", url2, data=payload, headers=headers, verify=False)
    TennantList = json.loads(response2.text)
    return TennantList


########################################################################################################################

########################################################################################################################
def create_l3out(tenant_name, l3out_name, VRF, physical_domain_name):
    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{l3out_name}.json'
    payload = F'{{"l3extOut":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}","name":"{l3out_name}","rn":"out-{l3out_name}","status":"created"}},"children":[{{"l3extRsEctx":{{"attributes":{{"tnFvCtxName":"{VRF}","status":"created"}},"children":[]}}}},{{"l3extRsL3DomAtt":{{"attributes":{{"tDn":"uni/l3dom-{physical_domain_name}","status":"created"}},"children":[]}}}}]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)


########################################################################################################################

########################################################################################################################
def create_l3_nodeprof(tenant_name, l3out_name, nodeprof_name):
    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}.json'
    payload = F'{{"l3extLNodeP":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}","name":"{nodeprof_name}","rn":"lnodep-{nodeprof_name}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)


########################################################################################################################

########################################################################################################################
def create_l3_logical_int(tenant_name, l3out_name, nodeprof_name, logical_int_name):
    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}/lifp-{logical_int_name}.json'
    payload = F'{{"l3extLIfP":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}/lifp-{logical_int_name}","name":"{logical_int_name}","rn":"lifp-{logical_int_name}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)


########################################################################################################################

########################################################################################################################
def create_l3_config_nodes(tenant_name, l3out_name, nodeprof_name, pod, node, router_id, ip_address, nexthop_address):
    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}/rsnodeL3OutAtt-[topology/pod-{pod}/node-{node}].json'
    payload = F'{{"l3extRsNodeL3OutAtt":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}/rsnodeL3OutAtt-[topology/pod-{pod}/node-{node}]","tDn":"topology/pod-{pod}/node-{node}","rtrId":"{router_id}","rn":"rsnodeL3OutAtt-[topology/pod-{pod}/node-{node}]","status":"created"}},"children":[{{"ipRouteP":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}/rsnodeL3OutAtt-[topology/pod-{pod}/node-{node}]/rt-[{ip_address}]","ip":"{ip_address}","rn":"rt-[{ip_address}]","status":"created"}},"children":[{{"ipNexthopP":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}/lnodep-{nodeprof_name}/rsnodeL3OutAtt-[topology/pod-{pod}/node-{node}]/rt-[{ip_address}]/nh-[{nexthop_address}]","nhAddr":"{nexthop_address}","rn":"nh-[{nexthop_address}]","status":"created"}},"children":[]}}}}]}}}}]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)


########################################################################################################################

########################################################################################################################
def create_l3_ext_network(tenant_name, l3out_name, ext_network_name, subnet_ip_address):
    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{l3out_name}/instP-{ext_network_name}.json'
    payload = F'{{"l3extInstP":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}/instP-{ext_network_name}","name":"{ext_network_name}","rn":"instP-{ext_network_name}","status":"created"}},"children":[{{"l3extSubnet":{{"attributes":{{"dn":"uni/tn-{tenant_name}/out-{l3out_name}/instP-{ext_network_name}/extsubnet-[{subnet_ip_address}]","ip":"{subnet_ip_address}","aggregate":"","rn":"extsubnet-[{subnet_ip_address}]","status":"created"}},"children":[]}}}}]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)

########################################################################################################################


########################################################################################################################
def disable_int_on_switch(pod, leaf, interface):
    url = F'https://{apicIP}/api/node/mo/uni/fabric/outofsvc.json'
    payload = F'{{"fabricRsOosPath":{{"attributes":{{"tDn":"topology/pod-{pod}/paths-{leaf}/pathep-[eth1/{interface}]","lc":"blacklist"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)
########################################################################################################################


########################################################################################################################
def enable_int_on_switch(pod, leaf, interface):
    url = F'https://{apicIP}/api/node/mo/uni/fabric/outofsvc.json'
    payload = F'{{"fabricRsOosPath":{{"attributes":{{"dn":"uni/fabric/outofsvc/rsoosPath-[topology/pod-{pod}/paths-{leaf}/pathep-[eth1/{interface}]]","status":"deleted"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)
########################################################################################################################


########################################################################################################################
def disable_vpc_on_switch(pod, leaf, VPC):
    url = F'https://{apicIP}/api/node/mo/uni/fabric/outofsvc.json'
    payload = F'{{"fabricRsOosPath":{{"attributes":{{"tDn":"topology/pod-{pod}/paths-{leaf}/pathep-[{VPC}]","lc":"blacklist"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)
########################################################################################################################


########################################################################################################################
def enable_vpc_on_switch(pod, leaf, VPC):
    url = F'https://{apicIP}/api/node/mo/uni/fabric/outofsvc.json'
    payload = F'{{"fabricRsOosPath":{{"attributes":{{"dn":"uni/fabric/outofsvc/rsoosPath-[topology/pod-{pod}/paths-{leaf}/pathep-[{VPC}]]","status":"deleted"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)
########################################################################################################################


########################################################################################################################
def create_contract(tenant_name, contract_name, **kwargs):
    if "subject" in kwargs["subject"]:
        if kwargs["subject"] != "":
            subject_name = kwargs["subject"]
            subject = F'{{"vzSubj":{{"attributes":{{"dn":"uni/tn-{tenant_name}/brc-{contract_name}","name":"{subject_name}","rn":"subj-{subject_name}","status":"created"}},"children":[]}}}}'
    else:
        subject = ""

    if kwargs["scope"].lower() == "ap":
        scope = ',"scope":"application-profile",'
    elif kwargs["scope"].lower() == "global":
        scope = ',"scope":"global",'
    elif kwargs["scope"].lower() == "tenant":
        scope = ',"scope":"tenant",'
    else:
        scope = ""

    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/brc-{contract_name}.json'
    payload = F'{{"vzBrCP":{{"attributes":{{"dn":"uni/tn-{tenant_name}/brc-{contract_name}","name":"{contract_name}"{scope}"rn":"brc-{contract_name}","status":"created"}},"children":[{subject}]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)
########################################################################################################################


########################################################################################################################
def add_contract_to_l3out_provider(tenant_name, l3out, ext_network_name, contract_name):
    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{l3out}/instP-{ext_network_name}.json'
    payload = F'{{"fvRsProv":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)

########################################################################################################################


########################################################################################################################
def add_contract_to_l3out_consumer(tenant_name, l3out, ext_network_name, contract_name):
    url = F'https://{apicIP}/api/node/mo/uni/tn-{tenant_name}/out-{l3out}/instP-{ext_network_name}.json'
    payload = F'{{"fvRsCons":{{"attributes":{{"tnVzBrCPName":"{contract_name}","status":"created"}},"children":[]}}}}'
    response = requests.request("POST", url, data=payload, headers=headers, verify=False)
    return json.loads(response.text)
########################################################################################################################


def create_VMM(vswitch_name, AEP, VLAN_pool, controller_name, vcentre_profile_name, userid, passwd, ip_address, dvs_version, data_centre):
    url = F'https://{apicIP}/api/node/mo/uni/infra.json'
    payload = F'{{"infraInfra":{{"attributes":{{"dn":"uni/infra","status":"created"}},"children":[{{"lacpLagPol":{{"attributes":{{"dn":"uni/infra/lacplagp-{vswitch_name}_lacpLagPol","name":"{vswitch_name}_lacpLagPol","status":"created","mode":"mac-pin"}},"children":[]}}}},{{"cdpIfPol":{{"attributes":{{"dn":"uni/infra/cdpIfP-{vswitch_name}_cdpIfPol","name":"{vswitch_name}_cdpIfPol","status":"created","adminSt":"enabled"}},"children":[]}}}},{{"lldpIfPol":{{"attributes":{{"dn":"uni/infra/lldpIfP-{vswitch_name}_lldpIfPol","name":"{vswitch_name}_lldpIfPol","status":"created","adminRxSt":"disabled","adminTxSt":"disabled"}},"children":[]}}}}]}}}}'
    response1 = requests.request("POST", url, data=payload, headers=headers, verify=False)

    url = F'https://{apicIP}/api/node/mo/uni/vmmp-VMware/dom-{vswitch_name}.json'
    payload = F'{{"vmmDomP":{{"attributes":{{"dn":"uni/vmmp-VMware/dom-{vswitch_name}","name":"{vswitch_name}","rn":"dom-{vswitch_name}","status":"created"}},"children":[{{"vmmCtrlrP":{{"attributes":{{"dn":"uni/vmmp-VMware/dom-{vswitch_name}/ctrlr-{controller_name}","name":"{controller_name}","hostOrIp":"{ip_address}","dvsVersion":"{dvs_version}","rootContName":"{data_centre}","rn":"ctrlr-{controller_name}","status":"created"}},"children":[{{"vmmRsAcc":{{"attributes":{{"tDn":"uni/vmmp-VMware/dom-{vswitch_name}/usracc-{vcentre_profile_name}","status":"created"}},"children":[]}}}}]}}}},{{"infraRsVlanNs":{{"attributes":{{"tDn":"uni/infra/vlanns-[{VLAN_pool}]-dynamic","status":"created"}},"children":[]}}}},{{"vmmUplinkPCont":{{"attributes":{{"dn":"uni/vmmp-VMware/dom-{vswitch_name}/uplinkpcont","numOfUplinks":"1","rn":"uplinkpcont","status":"created"}},"children":[]}}}},{{"vmmUsrAccP":{{"attributes":{{"dn":"uni/vmmp-VMware/dom-{vswitch_name}/usracc-{vcentre_profile_name}","name":"{vcentre_profile_name}","usr":"{userid}","pwd":"{passwd}","rn":"usracc-{vcentre_profile_name}","status":"created"}},"children":[]}}}},{{"vmmVSwitchPolicyCont":{{"attributes":{{"dn":"uni/vmmp-VMware/dom-{vswitch_name}/vswitchpolcont","status":"created"}},"children":[{{"vmmRsVswitchOverrideCdpIfPol":{{"attributes":{{"tDn":"uni/infra/cdpIfP-{vswitch_name}_cdpIfPol","status":"created"}},"children":[]}}}},{{"vmmRsVswitchOverrideLacpPol":{{"attributes":{{"tDn":"uni/infra/lacplagp-{vswitch_name}_lacpLagPol","status":"created"}},"children":[]}}}},{{"vmmRsVswitchOverrideLldpIfPol":{{"attributes":{{"tDn":"uni/infra/lldpIfP-{vswitch_name}_lldpIfPol","status":"created"}},"children":[]}}}}]}}}}]}}}}'
    response2 = requests.request("POST", url, data=payload, headers=headers, verify=False)

    url = F'https://{apicIP}/api/node/mo/uni/infra/attentp-{AEP}.json'
    payload = F'{{"infraRsDomP":{{"attributes":{{"tDn":"uni/vmmp-VMware/dom-{vswitch_name}","status":"created"}},"children":[]}}}}'
    response3 = requests.request("POST", url, data=payload, headers=headers, verify=False)

    return json.loads(response1.text), json.loads(response2.text), json.loads(response3.text)

