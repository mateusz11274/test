import apic_api
import csv
from datetime import datetime

########################################################################################
#name of the csv file to read from
csv_file_name = "VMM"

########################################################################################
with open(F'{csv_file_name}.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader)  # skips header line
    your_list = list(reader)

date = datetime.now().strftime("%Y-%m-%d_%H'%M'%S")
# log files
#This logfile contains all errors with "alredy exists"
log_already_exists = open(F'logScript_access_policies_already_exists_{date}.txt', "w+")
#Logfile for all other errors
log = open(F'logScript_access_policies_{date}.txt', "w+")

for i in your_list:
    vswitch_name, AEP, VLAN_pool, controller_name, vcentre_profile_name, userid, passwd, ip_address, dvs_version, data_centre = i
    response = str(apic_api.create_VMM(vswitch_name, AEP, VLAN_pool, controller_name, vcentre_profile_name, userid, passwd, ip_address, dvs_version, data_centre))
    if "already exists" in response:
        log_already_exists.write("Creating VMM\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating VMM\n")
        log.write(response + "\n")