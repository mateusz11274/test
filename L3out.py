"""
This script performs following steps
1. Create external l3 out
2. Create logical node profile
3. Create logical interface profile
4. Create SVI
5. Create configured nodes
6. Create external network

The script reads from CSV file that needs to be filled before execution, an example CSV file can be access from "l3out.csv"

2 Log scripts with a time stamp will be produced in local directory with names "logScript_l3out_"/"logScript_l3out_already_exists_" + curent time
The scirpts doesn't check if any of the policies already exisit. If the policy exisits already the script will not overwrite it and an error "...already exists" will apprear in the logfile.

logScript_l3out_already_exists will contain errors with "...already exists"
logScript_l3out will contain all other errors and responses with no errors
"""

import apic_api
import csv
from datetime import datetime

########################################################################################
#name of the csv file to read from
csv_file_name = "l3out"

########################################################################################

with open(F'{csv_file_name}.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader)  # skips header line
    your_list = list(reader)

date = datetime.now().strftime("%Y-%m-%d_%H'%M'%S")
# log file
#This logfile contains all errors with "alredy exists"
log_already_exists = open(F'logScript_l3out_already_exists_{date}.txt', "w+")
#Logfile for all other errors
log = open(F'logScript_l3out_{date}.txt', "w+")

for i in your_list:
    tenant_name, l3out_name, VRF, physical_domain_name, nodeprof_name, logical_int_name, pod, node, vpc, vlan, IP_sideA, IP_sideB, IP_secondary, router_id, ip_address, nexthop_address, ext_network_name, subnet_ip_address = i

    response = str(apic_api.create_l3out(tenant_name, l3out_name, VRF, physical_domain_name))

    if "already exists" in response:
        log_already_exists.write("Creating l3 out\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating l3 out\n")
        log.write(response + "\n")

    response = str(apic_api.create_l3_nodeprof(tenant_name, l3out_name, nodeprof_name))
    if "already exists" in response:
        log_already_exists.write("Creating logical node profile\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating logical node profile\n")
        log.write(response + "\n")

    response = str(apic_api.create_l3_logical_int(tenant_name, l3out_name, nodeprof_name, logical_int_name))
    if "already exists" in response:
        log_already_exists.write("Creating logical interface profile\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating logical interface profile\n")
        log.write(response + "\n")

    response = str(apic_api.createSVI(tenant_name, l3out_name, nodeprof_name, logical_int_name, pod, node, vpc, vlan, IP_sideA, IP_sideB, IP_secondary))
    if "already exists" in response:
        log_already_exists.write("Creating SVI\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating SVI\n")
        log.write(response + "\n")

    response = str(apic_api.create_l3_config_nodes(tenant_name, l3out_name, nodeprof_name, pod, node, router_id, ip_address, nexthop_address))
    if "already exists" in response:
        log_already_exists.write("Creating configured nodes\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating configured nodes\n")
        log.write(response + "\n")

    response = str(apic_api.create_l3_ext_network(tenant_name, l3out_name, ext_network_name, subnet_ip_address))
    if "already exists" in response:
        log_already_exists.write("Creating external network\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating external network\n")
        log.write(response + "\n")
