"""
This script perfroms following steps:
1. create Vlan pool
2. Create phys domain
3. attach phys domain to vlan pool
4. Create AEP
5. Attach AEP to physical domain
6. Create policy group
7. Create interface profile
8. Create port selector and attach policy group
9. create switch profile
10. Add leaf selector to switch profile
11. Associate interface profile to switch profile

The script reads from CSV file that needs to be filled before execution, an example CSV file can be access from "access_policies.csv"

2 Log scripts with a time stamp will be produced in local directory with names "logScript_access_policies_"/"logScript_access_policies_already_exists_" + curent time
The scirpts doesn't check if any of the policies already exisit. If the policy exisits already the script will not overwrite it and an error "...already exists" will apprear in the logfile.

logScript_access_policies_already_exists will contain errors with "...already exists"
logScript_access_policies will contain all other errors and responses with no errors

"""

import apic_api
import csv
from datetime import datetime
import apic_logger
import os

########################################################################################
#name of the csv file to read from
csv_file_name = "access_policies"
logging_enabled = "ON"

########################################################################################
with open(F'{csv_file_name}.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader)  # skips header line
    your_list = list(reader)

if logging_enabled == "ON":
    apic_logger.ACILogger.log_en = logging_enabled
    script_name = os.path.basename(__file__).split(".")[0]
    log2 = apic_logger.ACILogger(script_name)

date = datetime.now().strftime("%Y-%m-%d_%H'%M'%S")
# log files
#This logfile contains all errors with "alredy exists"
log_already_exists = open(F'logScript_access_policies_already_exists_{date}.txt', "w+")
#Logfile for all other errors
log = open(F'logScript_access_policies_{date}.txt', "w+")

for i in your_list:
    VLP_name, allocation_mode, VLP_range, Phy_name, AEP_name, policy_name, policy_type, CDP, LLDP, LACP, IntProfName, PortSelectorName, port_selector_block, SwitchProfName, SwitchSelectorName = i
    allocation_mode = allocation_mode.lower()

    Port = port_selector_block.split("/")[1]
    Switch = SwitchSelectorName.split("-")[1]

    if policy_type == "VPC":
        VPCPolGrpName = policy_name
        PolGrp = VPCPolGrpName
    else:
        VPCPolGrpName = ""

    if policy_type == "INT":
        IntPolGrpName = policy_name
        PolGrp = IntPolGrpName
    else:
        IntPolGrpName = ""

    if "-" in VLP_range:
        range_start, range_end = VLP_range.split("-")
    else:
        range_start = range_end = VLP_range

    response = str(apic_api.addVLP(VLP_name, allocation_mode, range_start, range_end))
    if "already exists" in response:
        log_already_exists.write("Creating VLAN pool \n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Creating VLAN pool \n")
        log.write(response+"\n")

    response = apic_api.createPHY(Phy_name)
    if "already exists" in response:
        log_already_exists.write("Creating physical domain \n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Creating physical domain \n")
        log.write(response+"\n")

    response = str(apic_api.attach_vlp_to_phy(Phy_name, VLP_name, allocation_mode))
    if "already exists" in response:
        log_already_exists.write("Adding VLAN pool to physical domain \n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Adding VLAN pool to physical domain \n")
        log.write(response+"\n")

    respone = apic_api.createAEP(AEP_name)
    if "already exists" in response:
        log_already_exists.write("Creating AEP\n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Creating AEP\n")
        log.write(response+"\n")

    response = str(apic_api.attachAEP(AEP_name, Phy_name))
    if "already exists" in response:
        log_already_exists.write("Attaching AEP to physical domain\n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Attaching AEP to physical domain\n")
        log.write(response+"\n")

    if policy_type == "VPC":
        respone = str(apic_api.createVPCPolGrp(VPCPolGrpName, AEP_name, CDP=CDP, LLDP=LLDP, LACP=LACP))
        if "already exists" in response:
            log_already_exists.write("Creating access/vpc policy group\n")
            log_already_exists.write(response + "\n")
        else:
            log.write("Creating access/vpc policy group\n")
            log.write(response + "\n")
    elif policy_type == "INT":
        response = str(apic_api.createIntPolGrp(IntPolGrpName, AEP_name, CDP=CDP, LLDP=LLDP))
        if "already exists" in response:
            log_already_exists.write("Creating access/vpc policy group\n")
            log_already_exists.write(response + "\n")
        else:
            log.write("Creating access/vpc policy group\n")
            log.write(response + "\n")

    response = str(apic_api.createIntProf(IntProfName))
    if "already exists" in response:
        log_already_exists.write("Creating Interface profile\n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Creating Interface profile\n")
        log.write(response+"\n")

    if policy_type == "VPC":
        response = str(apic_api.add_vpc_ptsel_to_int_prof(IntProfName, PortSelectorName, Port, PolGrp))
        if "already exists" in response:
            log_already_exists.write("Adding port selector to interface profile\n")
            log_already_exists.write(response + "\n")
        else:
            log.write("Adding port selector to interface profile\n")
            log.write(response + "\n")
    elif policy_type == "INT":
        response = str(apic_api.add_interface_ptsel_to_int_prof(IntProfName, PortSelectorName, Port, PolGrp))
        if "already exists" in response:
            log_already_exists.write("Adding port selector to interface profile\n")
            log_already_exists.write(response + "\n")
        else:
            log.write("Adding port selector to interface profile\n")
            log.write(response + "\n")

    response = (apic_api.createSwitchProf(SwitchProfName))
    if "already exists" in response:
        log_already_exists.write("Creating switch profile\n")
        log_already_exists.write(response + "\n")
    else:
        log.write("Creating switch profile\n")
        log.write(response + "\n")

    response = str(apic_api.AddLeafSeltoSwitchProf(SwitchProfName, SwitchSelectorName, Switch))
    if "already exists" in response:
        log_already_exists.write("Adding leaf to switch profile\n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Adding leaf to switch profile\n")
        log.write(response+"\n")

    response = str(apic_api.add_inteface_selector_to_switch_profile(SwitchProfName, IntProfName))
    task_name = "Adding interface profile to switch profile"
    if logging_enabled == "ON":
        log2.process_response(task_name, response)

    if "already exists" in response:
        log_already_exists.write("Adding interface profile to switch profile\n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Adding interface profile to switch profile\n")
        log.write(response+"\n")
