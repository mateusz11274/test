import json
import requests
import urllib3
import re
from apic_logger import ACILogger
import os

############################################################################
# Set logging on and off
logging_enabled = "ON"

############################################################################

if logging_enabled == "ON":
    ACILogger.log_en = logging_enabled
    script_name = os.path.basename(__file__).split(".")[0]
    log = ACILogger(script_name)


class Session:
    # Disables warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def __init__(self, apic_address, usr, password):
        if "https://" not in apic_address:
            if (":" or "/") in apic_address:
                # if 'https://' is misspelled
                print(F"{apic_address} is not a valid apic adress!")
                exit()
            else:
                ip = apic_address
                # add 'https://' to ip if missing
                apic_address = "https://" + apic_address
        else:
            # if 'https://' in string extract ip
            ip = apic_address.replace("https://", "")

        # only verifies the adress if ip is given rather than hostname
        if re.search(r'\d', ip):
            verify_ip = re.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
            if not verify_ip.match(ip):
                print(F"{apic_address} is not a valid apic adress!")

        self.apic_adress = str(apic_address)
        self.usr = str(usr)
        self.password = str(password)
        self.headers = self.log_in()

    def log_in(self):
        url = F"https://{self.apic_adress}/api/mo/aaaLogin.json"
        payload = F'{{"aaaUser":{{"attributes":{{"name":"{self.login}","pwd":"{self.password}"}}}}}}'
        response = json.loads(requests.request("POST", url, data=payload, verify=False).text)
        cookie = response['imdata'][0][0]['aaaLogin']['attributes']['token']
        headers = {'Content-Type': "application/json", 'Accept': "application/jsons",
                   'cookie': F"APIC-cookie={cookie}", 'Cache-Control': "no-cache"}
        return headers

    # Get data from APIC
    def get_from_apic(self, url):
        response = requests.request("GET", url, headers=self.headers, verify=False)
        ACILogger.process_response(response)
        return

    # Delete from method
    def delete_from_apic(self, url):
        response = requests.request("DELETE", url, headers=self.headers, verify=False)
        ACILogger.process_response(response)
        return

    # Post method for APIC
    def post_to_apic(self, url, payload):
        response = requests.request("POST", url, data=payload, headers=self.headers, verify=False)
        ACILogger.process_response(response)
        return
