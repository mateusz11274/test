import apic_logger
import os
import session

############################################################################
#Set logging on and off
logging_enabled = "ON"

############################################################################

if logging_enabled == "ON":
    apic_logger.ACILogger.log_en = logging_enabled
    script_name = os.path.basename(__file__).split(".")[0]
    log = apic_logger.ACILogger(script_name)


class ConfigureTenant:

    def __init__(self, tenant_name):
        self.url = F"/api/node/mo/uni/tn-{tenant_name}"

    def get_tenant_child(self, tenant_name, child_type, child_name):
        pass

    def create_tenant_child(self, tenant_name, child_type, child_name):
        if child_type.lower() == "ap":
            child_type = "fvAp"
            url = self.url + F"/ap-{child_name}"
        elif child_type.lower() == "bd":
            child_type = "fvBD"
            url = self.url + F"/BD-{child_name}"
        elif child_type.lower() == "vrf":
            child_type = "fvCtx"
            url = self.url + F"/ctx-{child_name}"
        elif child_type.lower() == "l3":
            child_type = "l3extOut"
            url = self.url + F"/out-{child_name}"
        else:
            print("invalid child name!")
            quit()


