import apic_logger
import os

############################################################################

logging_enabled = "ON"

############################################################################

if logging_enabled == "ON":
    apic_logger.ACILogger.log_en = logging_enabled
    script_name = os.path.basename(__file__).split(".")[0]
    log = apic_logger.ACILogger(script_name)

task_msg = "Creating physical domain"
response = {"totalCount": "1", "imdata": [{"error": {"attributes": {"code": "103", "text": "Cannot create Physical Domain (physDomP); object uni/phys-test_phy already exists."}}}]}

if logging_enabled == "ON":
    log.process_response(task_msg, response)
