"""
This scirpt performs following steps:
1. Create contract
2. Add contract to EPG as a provider
3. Add contract to EPG as a consumer

The script reads from CSV file that needs to be filled before execution, an example CSV file can be access from "contracts.csv"

2 Log scripts with a time stamp will be produced in local directory with names "logScript_contracts_"/"logScript_contracts_already_exists_" + curent time
The scirpts doesn't check if any of the policies already exisit. If the policy exisits already the script will not overwrite it and an error "...already exists" will apprear in the logfile.

logScript_contracts_already_exists will contain errors with "...already exists"
logScript_contracts will contain all other errors and responses with no errors
"""

import apic_api
import csv
from datetime import datetime

########################################################################################
#name of the csv file to read from
csv_file_name = "contracts"

########################################################################################
with open(F'{csv_file_name}.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader)  # skips header line
    your_list = list(reader)

date = datetime.now().strftime("%Y-%m-%d_%H'%M'%S")
# log file
#This logfile contains all errors with "alredy exists"
log_already_exists = open(F'logScript_contracts_already_exists_{date}.txt', "w+")
#Logfile for all other errors
log = open(F'logScript_contracts_{date}.txt', "w+")

for i in your_list:
    contract_name, scope, subject, tenant_name, ap_profile_provider, epg_provider, ap_profile_consumer, epg_consumer = i

    response = str(apic_api.create_contract(tenant_name, contract_name, scope=scope, subject=subject))
    if "already exists" in response:
        log_already_exists.write("Creating contract\n")
        log_already_exists.write(response+"\n")
    else:
        log.write("Creating contract\n")
        log.write(response+"\n")

    response = str(apic_api.AssociateEPGasProvider(tenant_name, ap_profile_provider, epg_provider, contract_name))
    if "already exists" in response:
        log_already_exists.write(F"Adding contract to EPG: {epg_provider} as provider \n")
        log_already_exists.write(response+"\n")
    else:
        log.write(F"Adding contract to EPG: {epg_provider} as provider \n")
        log.write(response+"\n")

    response = str(apic_api.AssociateEPGasConsumer(tenant_name, ap_profile_consumer, epg_consumer, contract_name))
    if "already exists" in response:
        log_already_exists.write(F"Adding contract to EPG: {epg_consumer} as consumer \n")
        log_already_exists.write(response+"\n")
    else:
        log.write(F"Adding contract to EPG: {epg_consumer} as consumer \n")
        log.write(response+"\n")

