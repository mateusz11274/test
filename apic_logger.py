""""
Classs for logging ACI tasks and responses

Created by: Mateusz Janowski
Email: mateusz.janowski@bt.com


Add the following code to script:
############################################################################
#Set logging on and off
logging_enabled = "ON"

############################################################################

if logging_enabled == "ON":
    apic_logger.ACILogger.log_en = logging_enabled
    script_name = os.path.basename(__file__).split(".")[0]
    log = apic_logger.ACILogger(script_name)


"""

import logging
from datetime import datetime
import os


class ACILogger:
    log_en = "OFF"  # logging disabled by default
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    status_code_dict = {"200": "OK", "201": "Created", "204": "No Content", "400": "Bad Request", "403": "Unauthorised",
                        "404": "Not Found", "409": "Conflict"}
    date = datetime.now().strftime("%Y-%m-%d_%H'%M'%S")

    #creates 'logs' folder in local directory
    path = os.getcwd()
    os.makedirs(path + "\logs", exist_ok=True)

    def __init__(self, name):
        self.name = name
        self.logger_errors = ACILogger.set_up_logger("Errors",
                                                     F"{self.path}\\logs\\{self.name}_logscript_errors_{ACILogger.date}.log",
                                                     level=logging.ERROR)
        self.logger_detailed = ACILogger.set_up_logger("Detailed",
                                                       F"{self.path}\\logs\\{self.name}_logscript_detailed_{ACILogger.date}.log",
                                                       level=logging.DEBUG)

    @staticmethod
    def set_up_logger(name, log_file, formatter=formatter, level=logging.DEBUG):
        handler = logging.FileHandler(log_file)
        handler.setFormatter(formatter)
        logger = logging.getLogger(name)
        logger.setLevel(level)
        logger.addHandler(handler)
        return logger

    def process_response(self, response, task_msg=None):
        status_code = response.status_code
        message = response.text
        if status_code[0] == "2":
            self.logger_detailed.debug(task_msg)
            self.logger_detailed.debug("Status code: " + status_code + " " + self.status_code_dict[status_code])
        else:
            error_code = message["imdata"][0]["error"]["attributes"]["code"]
            error_message = message["imdata"][0]["error"]["attributes"]["text"]
            if error_code == "103":
                self.logger_detailed.warning(task_msg)
                self.logger_detailed.warning(
                    "Status code: " + status_code + " " + self.status_code_dict[status_code] + ". " + error_message)
            else:
                self.logger_detailed.error(task_msg)
                self.logger_detailed.error(
                    "Status code: " + status_code + " " + self.status_code_dict[status_code] + ". " + error_message)
                self.logger_errors.error(task_msg)
                self.logger_errors.error(
                    "Status code: " + status_code + " " + self.status_code_dict[status_code] + ". " + error_message)
        return

    def write_to_log(self, msg):
        self.logger_detailed.info(msg)
        return



